<h1># GitHubApi</h1>
<h2>Arquitetura:</h2><br />
Para o Desenvolvimento foi utilizado o padrão MVVM com o auxilio do Android Jetpack. O desenvolvimento a utilização de uma arquitetura baseada em modularização (modulos ou bibliotecas no Androiod), ou seja, a aplicação foi divida em 3 modulos (serviços), são eles:
<p><b>remote -></b> responsavel pela comunicação com a Api com o auxilio da biblioteca Retrofit; 
<p><b>app -></b> o modulo responsável pela parte das Views, estando no topo da hierarquia por se comunicar com os 2 outros modulos. Esse modulo é o responsável por instanciar a aplicação, realizar a gerenciar as injeções de dependências com Koin, ViewModel que interagem com a camada de repositorio e com a camada da View.





<h2>Libs:</h2><br />

<p><b>coroutines -></b> lib utilizada para chamadas async tanto para o banco de dados local como para as requisições remotas, além disso foi utilizada a lib para testes unitários;
<p><b>Koin -></b> Utilizado para injeção de dependencias entre os componentes, a lib é usada para injeção de depência de viewmodels, repositórios, SharedPreferences, Room, e testes unitários;
<p><b>Glide -></b> Usado para carrengamento e caching de imagens;
<p><b>Lottie -></b> Usada para animação do loading, essa poderosa biblioteca da Airbnb é ótima e de fácil utilização, utilizei apenas para questão de estética;
<p><b>Gson -></b> Utilizado para converter os resultados das chamadas da API para objetos;
<p><b>Retrofit -></b> Utilizada para interação com a API do projeto;
<p><b>Orangegangsters -></b> Utilizada para facilitar o carregametno de novos conteudos na lista;


<h2>Tests:</h2><br />

<p>Foi criado os testes da ViewModel responsavel pela comunicação entre a tela que mostra a lista de repositorios, todas as chamadas bem como erros que podem ocorrer com a falta de conexão com a internet por exemplo.

<h2>Continuous Integration CI:</h2>
<p>Utilizei um arquivo .yaml para configurar uma esteira para o projeto que será rodada sempre que uma nova Branch seja mergeada com a branch develop garantindo assim que os testes unitários, Lint e o build estejam sempre rodando, caso contrário o erro pode ser pego e corrigido o quanto antes</p>
