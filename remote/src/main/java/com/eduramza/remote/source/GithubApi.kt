package com.eduramza.remote.source

import com.eduramza.remote.model.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {

    @GET(LIST_ROUTE)
    suspend fun listSortByStar(@Query("q") language: String = "language:kotlin",
                               @Query("sort") sort: String = "star",
                               @Query("page")page : String,
                               @Query("per_page") per_page : String = "20"): Response

}