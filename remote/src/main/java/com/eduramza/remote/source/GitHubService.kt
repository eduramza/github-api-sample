package com.eduramza.remote.source

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val LIST_ROUTE = "search/repositories"
const val BASE_URL = "https://api.github.com/"

class GitHubService {

    fun getRemoteService(): GithubApi{
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(GithubApi::class.java)
    }
}