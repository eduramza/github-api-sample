package com.eduramza.remote.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eduramza.remote.model.Response

interface GithubRepository {
    suspend fun callRepositories(page: String): List<Response.Item>
    fun getErrors(): MutableLiveData<Boolean>
}