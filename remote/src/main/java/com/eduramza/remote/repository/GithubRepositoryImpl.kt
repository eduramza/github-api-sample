package com.eduramza.remote.repository

import androidx.lifecycle.MutableLiveData
import com.eduramza.remote.model.Response
import com.eduramza.remote.source.GithubApi

class GithubRepositoryImpl(private val api: GithubApi): GithubRepository{

    private val errorGeneric = MutableLiveData<Boolean>()
    override fun getErrors() = errorGeneric

    override suspend fun callRepositories(page: String): List<Response.Item> {
        return try{
            api.listSortByStar(page = page).items
        } catch (e: Exception){
            errorGeneric.postValue(true)
            emptyList()
        }
    }

}