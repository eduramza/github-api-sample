package com.eduramza.githubapiexample.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.eduramza.githubapiexample.R
import com.eduramza.remote.model.Response
import kotlinx.android.synthetic.main.item_list_github_projects.view.*

class ProjectsAdapter(private val context: Context?,
                      private val repositories: MutableList<Response.Item>,
                      private val adapterListener: MyAdapterListener):
    RecyclerView.Adapter<ProjectsAdapter.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(parent.inflate(R.layout.item_list_github_projects))
    }

    override fun getItemCount() = repositories.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindView(repositories[position])
    }

    fun updateList(repositories: MutableList<Response.Item>){
        this.repositories.clear()
        this.repositories.addAll(repositories)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private lateinit var item: Response.Item

        fun bindView(item: Response.Item){
            this.item = item

            createRoundedImage()
            itemView.tv_list_repository.text = item.name
            itemView.tv_list_author.text = item.owner.login
            itemView.tv_qtd_stars.text = item.stargazersCount.toString()
            itemView.tv_qtd_forks.text = item.forksCount.toString()

            itemView.card_container_list.setOnClickListener {
                adapterListener.onItemClick(item)
            }

        }

        private fun createRoundedImage(){
            context?.let {
                Glide.with(it)
                    .load(item.owner.avatarUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.img_list_profile)
            }
        }

    }

    interface MyAdapterListener{
        fun onItemClick(item: Response.Item)
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}