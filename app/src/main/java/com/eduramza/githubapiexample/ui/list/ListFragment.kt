package com.eduramza.githubapiexample.ui.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.eduramza.githubapiexample.DETAIL_FRAGMENT
import com.eduramza.githubapiexample.R
import com.eduramza.githubapiexample.ui.adapter.ProjectsAdapter
import com.eduramza.githubapiexample.ui.details.DetailsFragment
import com.eduramza.remote.model.Response
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class ListFragment : Fragment(), ProjectsAdapter.MyAdapterListener {

    companion object {
        fun newInstance() = ListFragment()
    }

    private val listViewModel: ListViewModel by viewModel()
    private lateinit var adapter: ProjectsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configList()
        configObservers()
        listViewModel.callRepositories()
        refreshers()
    }

    private fun refreshers(){
        swipeRefresh.setOnRefreshListener {
            listViewModel.incrementPage()
            swipeRefresh.isRefreshing = false
        }
    }

    private fun configList(){
        adapter = ProjectsAdapter(context, mutableListOf(), this)
        list_github_projects.layoutManager = LinearLayoutManager(context)
        list_github_projects.itemAnimator = DefaultItemAnimator()
        list_github_projects.adapter = adapter
    }

    private fun configObservers(){
        listViewModel.getResponse().observe(this, Observer { result ->
            adapter.updateList(result as MutableList<Response.Item>)
        })
        listViewModel.getError().observe(this, Observer {
            if (it) { showError() }
        })
    }

    override fun onItemClick(item: Response.Item) {
        openDetails(item)
    }

    private fun showError(){
        swipeRefresh.visibility = GONE
        layout_error.visibility = VISIBLE
        bt_error.setOnClickListener {
            activity!!.finish()
        }
    }

    private fun openDetails(item: Response.Item) {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.container, DetailsFragment.newInstance(item), DETAIL_FRAGMENT)
            ?.commit()
    }

}
