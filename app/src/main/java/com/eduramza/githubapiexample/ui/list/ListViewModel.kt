package com.eduramza.githubapiexample.ui.list

import androidx.lifecycle.*
import com.eduramza.remote.model.Response
import com.eduramza.remote.repository.GithubRepository
import kotlinx.coroutines.*
import java.io.File

class ListViewModel(private val repository: GithubRepository) : ViewModel() {

    fun getError() = repository.getErrors()
    fun getResponse() = response

    private val response = MutableLiveData<MutableList<Response.Item>>()

    var page = 1
    var listOfRepositories : MutableList<Response.Item> = mutableListOf()

    fun callRepositories() {

        listOfRepositories.run {
            runBlocking (Dispatchers.IO) {
                this@run.addAll( repository.callRepositories(page.toString()))
            }
        }
        response.postValue(listOfRepositories)
    }

    fun incrementPage() {
        page += 1
        callRepositories()
    }

}
