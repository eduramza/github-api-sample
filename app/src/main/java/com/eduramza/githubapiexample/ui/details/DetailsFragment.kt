package com.eduramza.githubapiexample.ui.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.eduramza.githubapiexample.R
import com.eduramza.remote.model.Response
import kotlinx.android.synthetic.main.details_fragment.*
class DetailsFragment : Fragment() {

    companion object {
        lateinit var items : Response.Item

        fun newInstance(items : Response.Item): DetailsFragment{
            this.items = items
            return DetailsFragment()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        bindFragment()
    }

    private fun bindFragment(){
        configImageProfile()
        tv_repository_name.text = items.name
        tv_author_name.text = items.owner.login
        tv_qtd_stars.text = items.stargazersCount.toString()
        tv_qtd_forks.text = items.forksCount.toString()
        tv_description.text = items.description

        bt_link_github.setOnClickListener {
            openGitHub()
        }
    }

    private fun configImageProfile(){
        context?.let {
            Glide.with(it)
                .load(items.owner.avatarUrl)
                .into(img_profile)
        }
    }

    private fun openGitHub(){
        val uri = Uri.parse(items.htmlUrl)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        activity?.startActivity(intent)
    }

}
