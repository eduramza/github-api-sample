package com.eduramza.githubapiexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.eduramza.githubapiexample.ui.list.ListFragment


const val LIST_FRAGMENT = "LIST_FRAGMENT"
const val DETAIL_FRAGMENT = "DETAIL_FRAGMENT"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            attachListFragment()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            navigateBetweenFragments()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigateBetweenFragments()
    }

    private fun navigateBetweenFragments(){
        if (supportFragmentManager.findFragmentByTag(DETAIL_FRAGMENT) != null){
            attachListFragment()
        } else {
            finish()
        }
    }

    private fun attachListFragment(){
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ListFragment.newInstance(), LIST_FRAGMENT)
            .commitNow()
    }

}
