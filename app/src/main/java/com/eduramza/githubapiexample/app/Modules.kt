package com.eduramza.githubapiexample.app

import com.eduramza.githubapiexample.ui.list.ListViewModel
import com.eduramza.remote.repository.GithubRepository
import com.eduramza.remote.repository.GithubRepositoryImpl
import com.eduramza.remote.source.GitHubService
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    viewModel { ListViewModel(get()) }
}

val remoteModule = module{
    single { GitHubService().getRemoteService() }

    single<GithubRepository> { GithubRepositoryImpl(get()) }

}