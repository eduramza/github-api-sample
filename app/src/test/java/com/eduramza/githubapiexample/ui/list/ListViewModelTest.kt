package com.eduramza.githubapiexample.ui.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.eduramza.githubapiexample.ManagedCoroutineScope
import com.eduramza.githubapiexample.TestScope
import com.eduramza.remote.model.Response
import com.eduramza.remote.repository.GithubRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.File

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ListViewModelTest: KoinTest{

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val managedCoroutineScope: ManagedCoroutineScope = TestScope(testDispatcher)

    private val mockResponse = "response/mock_response"
    private var type = object : TypeToken<Response>() {

    }.type


    private val listViewModel: ListViewModel by inject()

    @Mock
    private lateinit var repository: GithubRepository

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)

        startKoin {
            modules(
                module {
                    single { ListViewModel(repository) }
                }
            )
        }
    }

    @After
    fun after(){
        stopKoin()
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun testCallRepository() = runBlockingTest {

        initMockRepository()

        //action
        launch(managedCoroutineScope.coroutineContext) { listViewModel.callRepositories() }
        verify(repository).callRepositories(anyString())

    }

    @Test
    fun testVerifyResponseIsNotNull() = runBlockingTest {

        initMockRepository()

        //action
        launch(managedCoroutineScope.coroutineContext) { listViewModel.callRepositories() }
        val response = listViewModel.getResponse()

        verify(repository).callRepositories(anyString())

        assertNotNull(response.value)

    }

    @Test
    fun testVerifyListUpdatedTwoTimes() = runBlockingTest {

        initMockRepository()

        //action
        launch(managedCoroutineScope.coroutineContext) { listViewModel.callRepositories() }
        listViewModel.incrementPage()

        val response = listViewModel.listOfRepositories

        verify(repository, times(2)).callRepositories(anyString())

        assertEquals(60, response.size)

    }

    @Test
    fun testIncrementPage() {

        initMockRepository()

        //action
        listViewModel.incrementPage()
        val result = listViewModel.page

        assertEquals(2, result)
    }

    @Test
    fun testSequenceIncrement() {

        initMockRepository()

        listViewModel.incrementPage()
        listViewModel.incrementPage()
        listViewModel.incrementPage()

        val result: Int = listViewModel.page

        assertEquals(4, result)
    }

    private fun initMockRepository() = runBlockingTest {
        val data = getJson(mockResponse)
        val model = Gson().fromJson<Response>(data, type)

        whenever(repository.callRepositories(anyString())).thenReturn(model.items)

    }

    private fun getJson(path : String) : String {
        val uri = this.javaClass.classLoader?.getResource(path)
        val file = File(uri!!.path)
        return String(file.readBytes())
    }

}